(do
  ; Author: Muresan Vlad Mihail
  ; Date: 03.05.2018
  ; Description: File which helps you make animations in CLove using Aria

  (= anim:img (function (v) (vector-get v 0)))
  (= anim:w (function (v) (vector-get v 1)))
  (= anim:h (function (v) (vector-get v 2)))
  (= anim:fx (function (v) (vector-get v 3)))
  (= anim:fy (function (v) (vector-get v 4)))
  (= anim:frames (function (v) (vector-get v 5)))
  (= anim:img-w (function (v) (vector-get v 6)))
  (= anim:img-h (function (v) (vector-get v 7)))
  (= anim:curr-frame (function (v) (vector-get v 8)))
  (= anim:curr-speed (function (v) (vector-get v 9)))
  (= anim:loop (function (v) (vector-get v 10)))
  (= anim:stop (function (v) (vector-get v 11)))
  (= anim:custom (function (v) (vector-get v 12)))
  (= anim:stop-frame (function (v) (vector-get v 13)))
  (= anim:start-frame (function (v) (vector-get v 14)))
  (= anim:f (function (v) (vector-get v 15)))
  (= anim:speed (function (v) (vector-get v 16)))

  (= anim:set-img (function (v val) (vector-set v 0 val)))
  (= anim:set-w (function (v val) (vector-set v 1 val)))
  (= anim:set-h (function (v val) (vector-set v 2 val)))
  (= anim:set-fx (function (v val) (vector-set v 3 val)))
  (= anim:set-fy (function (v val) (vector-set v 4 val)))
  (= anim:set-frames (function (v val) (vector-set v 5 val)))
  (= anim:set-img-w (function (v val) (vector-set v 6 val)))
  (= anim:set-img-h (function (v val) (vector-set v 7 val)))
  (= anim:set-curr-frame (function (v val) (vector-set v 8 val)))
  (= anim:set-curr-speed (function (v val) (vector-set v 9 val)))
  (= anim:set-loop (function (v val) (vector-set v 10 val)))
  (= anim:set-stop (function (v val) (vector-set v 11 val)))
  (= anim:set-custom (function (v val) (vector-set v 12 val)))
  (= anim:set-stop-frame (function (v val) (vector-set v 13 val)))
  (= anim:set-start-frame (function (v val) (vector-set v 14 val)))
  (= anim:set-f (function (v val) (vector-set v 15 val)))
  (= anim:set-speed (function (v val) (vector-set v 16 val)))


  ; Image you want to use
  ; Width/Height of the frame
  (= anim:init
     (function (img, width, height)
               (vector
                 img width height 0 0 (vector)
                 (love:graphics-image-getWidth img) (love:graphics-image-getHeight img)
                 0 0 t 0 0 0 0 0 0 0)))

  (= anim:_make-frame
     (function (vec)
               (when (eq (anim:f vec) 0)
                 (anim:set-f vec (floor (* (/ (anim:img-w vec) (anim:w vec))
                                              (/ (anim:img-h vec) (anim:h vec))))))

               (anim:set-fx vec (floor (/ (anim:img-w vec) (anim:w vec))))
               (let (i 1 row 0 column 0 frames (vector))
                 (loop (< i (anim:f vec))
                   (= row (floor (/ (- i 1) (anim:fx vec))))
                   (= column (mod (- i 1) (anim:fx vec)))
                   (vector-push
                     frames (love:graphics-newQuad (* column (anim:w vec)) (* row (anim:h vec))
                                                              (anim:w vec) (anim:h vec) (anim:img-w vec) (anim:img-h vec)))
                   (= i (+ i 1)))
                   (anim:set-frames vec frames)
                 )))

  (= anim:add
     (function (vec startFrame endFrame frame-speed)
               (unless frame-speed (= frame-speed 0))
               (anim:set-speed vec frame-speed)
               (anim:_make-frame vec)
               (anim:set-stop-frame vec endFrame)
               (anim:set-start-frame vec startFrame)))


  ;(print "Called: " (anim:frames vec) (anim:curr-frame vec) (car (vector-get (anim:frames vec) (anim:curr-frame vec))))
  (= anim:draw
     (function (vec, x, y, rot, sx, sy, kx, ky)
               (unless rot (= rot 0))
               (unless sx (= sx 1))
               (unless sy (= sy 1))
               (unless kx (= kx 0))
               (unless ky (= ky 0))

               (love:graphics-drawQuad (anim:img vec)
               (vector-get (anim:frames vec) (anim:curr-frame vec)) x y rot sx sy kx ky)))

  (= anim:play
     (function (vec repeat)
               (unless (eq (anim:stop vec) t)
                 (anim:set-loop vec repeat)
                 (anim:set-curr-speed vec (+ (anim:curr-speed vec) (love:timer-getDelta)))
                 (cond
                   ; First condition
                   (> (anim:speed vec) 0)
                   (do
                     (when (and (eq anim:curr-frame 0) (>= anim:start-frame 1))
                       (anim:set-curr-frame vec (anim:start-frame vec)))
                     (when (>= (anim:curr-speed vec) (anim:speed vec))
                       (anim:set-curr-frame vec (+ (anim:curr-frame vec) 1))
                       (anim:set-curr-speed vec 0)))
                   ; Second condition
                   (eq (anim:speed vec) 0) (anim:curr-frame vec (anim:start-frame vec)))

                 [ if (endp (anim:stop-frame vec))
                   (when (> (anim:curr-frame vec) (vector-length (anim:frames vec)))
                        (if (anim:loop vec)
                          (if (>= (anim:start-frame vec) 1)
                            (anim:set-curr-frame vec (anim:start-frame vec))
                            (anim:curr-frame vec 1))
                          (anim:set-curr-frame (vector-length (anim:frames vec))))
                     ) ;when
                   (do
                   (when (>= (anim:curr-frame vec) (anim:stop-frame vec))
                     (if repeat
                       (when (>= (anim:start-frame vec) 0)
                         (anim:set-curr-frame vec (anim:start-frame vec)))
                       (anim:set-curr-frame vec (- (anim:stop-frame vec) 1)))))
                   ] ;if
                 ) ;when
               ))

  (= anim:reach-end
     (function (vec)
               (if (>= (anim:curr-frame vec)  (anim:stop-frame vec))
                 (t)
                 (nil))))

  (= anim:change-start-frame
     (function (vec start)
               (anim:set-start-frame vec start)
               (anim:set-curr-frame vec start)))

  (= anim:change-stop-frame
     (function (vec stop)
               (anim:set-stop-frame vec stop)))

  (= anim:get-length
     (function (vec stop)
               (vector-length (anim:frames vec))))

  (= anim:pause
     (function (vec)
               (anim:set-stop vec t)))

  (= anim:resume
     (function (vec)
               (anim:set-stop vec 0)))

  (= anim:free
   (function (vec)
    (let (i 'it)
      (dovector (i (anim:frames vec))
       (love:graphics-quad-gc  it))
    )))
 )
