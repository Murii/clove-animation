Usage
-------

Simple and easy to use library for animations
written in Lua for (C)Love.

For documentation see anim.lua

```lua
require "anim"

    function love.load()
        -- image to load, width of frame, height of frame
        currentAnim = aim.create(love.graphics.newImage("myimg.png"), 16, 16)
        -- startFrame, stopFrame, speed
        currentAnim:add(1, 3, .3)
    end

    function love.update(dt)
        -- pass true if you want it to be looped
        currentAnim:play()
    end

    function love.draw()
        -- x, y, rot, sx, sy, kx, ky
        currentAnim:draw(100, 100)
    end

```

```cl
(do
  (require 'anim.lisp)
  (love:window-setMode 800 600)

  (= love-load
   (function ()
    (= image (love:graphics-newImage "quad.png"))

    (= a (anim:create image 8 16))
    (anim:add a 1 3 0.5)

    (= b (anim:create image 8 16))
    (anim:add b 1 3 1.5)
   ))

  (= love-update
   (function (dt)
		(anim:play a t)
		(anim:play b t)
   ))

  (= love-draw
   (function ()
		(anim:draw a 200 200 0 1 1 0 0)
		(anim:draw b 250 250 0 1 1 0 0)
    ))

  (= love-quit
   (function ()
    (anim:free a)
    (anim:free b)))
)
```
